
function connector(_class) {
    return function (req, res, next) {
        new _class(req, res, next)
    }
}

module.exports = connector;