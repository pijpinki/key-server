class VarsDelete {
    constructor() {
        this.keysToDelete = [];
    }

    getVars() {
        for (const key in global.vars)
            if (global.vars[key].expires < new Date())
                this.keysToDelete.push(key);
    }

    deleteVars() {
        console.log('Delete vars', this.keysToDelete);

        for(const key of this.keysToDelete)
            delete global.vars[key];
    }

    clearVars() {
        this.keysToDelete = [];
    }

    main() {
        this.getVars();
        this.deleteVars();
        this.clearVars();

        return setTimeout(() => this.main(), 30 * 1000);
    }
}

module.exports = VarsDelete;