import io from 'socket.io-client';

class Api {
    /**
     * Socket api module
     * @param {string} url - http://localhost:19001
     */
    constructor(url) {
        this.url = url;


        /**
         * @type {socket}
         */
        this.socket = io(this.url);

        this.connected    = false;
        this.statusLister = () => console.log();

        this.init();
    }

    setStatusLister(clb) {
        this.statusLister = clb;
    }

    init() {
        this.socket.on('disconnect', () => {
            this.connected = false;
            this.statusLister(null, this.connected);
        });

        this.socket.on('connect', () => {
            this.connected = true;
            this.statusLister(null, this.connected);
        });
    }

    /**
     * Send emit
     * @param {string} name - Login -> LoginRequest
     * @param {*} data - Data
     */
    send(name, data) {
        this.socket.emit(name + 'Request', data);
    }

    /**
     * Listen emit
     * @param {string} name - Login -> LoginResponse
     * @param {clb} clb
     */
    on(name, clb) {
        this.socket.on(name + 'Response', data => clb(null, data));
    }

    /**
     * Off listen emit
     * @param name - Login -> LoignResponse
     */
    off(name) {
        this.socket.off(name + 'Response');
    }

    /**
     * Send Request NameRequest NameResponse
     * @param {string} name - Login
     * @param {*} data - Data
     * @param {clb} clb
     */
    request(name, data, clb) {
        this.send(name, data);

        this.on(name, (err, data) => {
            console.log(name, data);
            this.off(name);
            this.off('Error');

            return clb(null, data);
        });

        this.on('Error', (e, error) => {
            console.error('Error', error);
            this.off(name);
            this.off('Error');

            return clb(error);
        });
    }
}

/**
 * make address for api
 * @return {string}
 */
function makeApiAddress() {
    const url = window.location.host;

    if (url.indexOf('localhost') !== -1)
        return 'http://localhost:19001';

    if (url.indexOf('ixat') !== -1)
        return 'http://api.ixat.taxi:19001';

    return 'http://loclhost:19001';
}

export default new Api(makeApiAddress());

/**
 * @typedef {Object} socket
 * @property {function} emit - Отрпавить событие
 * @property {function} on - Слушать событие
 * @property {function} off - Перестать слушать событие
 */