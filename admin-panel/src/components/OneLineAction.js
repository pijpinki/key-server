import PropTypes from 'prop-types';
import React from 'react';

class OneLineAction extends React.Component {
    static propTypes = {
        actionLabel: PropTypes.string.isRequired,
        label      : PropTypes.string.isRequired,
        onApply    : PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            name        : '',
            errorMessage: ''
        };
    }

    /**
     * on change name input
     * @param {event} event
     */
    onChangeName(event) {
        this.setState({name: event.target.value});
    }

    onApply() {
        const {name} = this.state;

        if (!name)
            return this.setState({errorMessage: 'Check input'});

        this.props.onApply({name});
    }

    render() {
        return (
            <div>
                <div className='form'>
                    <span>{this.props.label}</span>

                    {this.state.errorMessage && <span className='text-danger'>{this.state.errorMessage}</span>}

                    <div className='form-group'>
                        <input className='form-control' type='text' placeholder='name'
                               onChange={this.onChangeName.bind(this)}/>
                    </div>

                    <button className='btn btn-primary w-100'
                            onClick={this.onApply.bind(this)}>{this.props.actionLabel}</button>
                </div>
            </div>
        );
    }
}

export default OneLineAction;
