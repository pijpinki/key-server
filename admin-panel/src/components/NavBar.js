import React from 'react';

import api from '../modules/api';

class NavBar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            connected: false
        };
    }

    componentWillMount() {
        api.setStatusLister((err, conntected) => {
            this.changeConnectedStatus(conntected);
        });
    }

    componentWillUnmount() {
        api.setStatusLister(() => null);
    }

    changeConnectedStatus(connected) {
        this.setState({connected});
    }

    render() {
        return (
            <div>
                <nav className="navbar navbar-expand navbar-light bg-light">
                    <a className="navbar-brand">Navbar</a>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav">
                            <li className="nav-item active">
                                <a className="nav-link">Home</a>
                            </li>
                        </ul>
                        <div className={`btn ${this.state.connected ? 'btn-primary' : 'btn-warning'}`}>
                            {this.state.connected ? 'ONLINE' : 'OFFLINE'}
                        </div>
                    </div>
                </nav>
            </div>
        );
    }
}

export default NavBar;