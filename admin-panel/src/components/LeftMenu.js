import React from 'react';

import VarAction from './VarAction';
import OneLineAction from './OneLineAction';

import SuccessModal from './modals/SuccessModal';
import ErrorModal from './modals/ErrorModal';

import api from '../modules/api';

class LeftMenu extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            errorModal  : false,
            errorMessage: '',

            successModal: false
        };


        this.modalTimeout = 3 * 1000;
    }

    onAddVar(input) {
        api.request('AddVar', input, (err) => {
            if (err)
                return this.error(err);

            this.success();
        });
    }

    onEditVar(input) {
        api.request('EditVar', input, err => {
            if (err)
                this.error(err);

            this.success();
        });
    }

    onDeleteVar(input) {
        api.request('DeleteVar', input, err => {
            if (err)
                return this.error(err);

            this.success();
        });
    }

    success() {
        this.setState({successModal: true}, () => {
            setTimeout(() => this.setState({successModal: false}), this.modalTimeout);
        });
    }

    error(error) {
        this.setState({errorModal: true, errorMessage: error.message}, () => {
            setTimeout(() => this.setState({errorModal: false}), this.modalTimeout);
        });
    }

    render() {
        return (
            <div className='col-4'>
                <div className='card'>
                    <div className='card-body'>
                        <div>
                            <VarAction actionLabel='Add' label='Add new var' onApply={this.onAddVar.bind(this)}/>
                            <hr/>

                            <VarAction actionLabel='Edit' label='Edit old var' onApply={this.onEditVar.bind(this)}/>
                            <hr/>

                            <OneLineAction actionLabel='Delete' label='Delete one var'
                                           onApply={this.onDeleteVar.bind(this)}/>
                            <hr/>

                        </div>
                    </div>
                </div>
                <SuccessModal open={this.state.successModal}/>
                <ErrorModal open={this.state.errorModal} message={this.state.errorMessage}/>
            </div>
        );
    }
}

export default LeftMenu;