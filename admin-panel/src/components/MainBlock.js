import React from 'react';
import Vars from './Vars';
import LeftMenu from './LeftMenu';

class MainBlock extends React.Component {
    render() {
        return (
            <div className='row'>
                <LeftMenu/>
                <Vars/>
            </div>
        );
    }
}

export default MainBlock;