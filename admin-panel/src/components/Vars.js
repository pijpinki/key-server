import React from 'react';

import api from '../modules/api';

class Vars extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            errorMessage: '',
            vars        : []
        };
    }

    componentWillMount() {
        api.request('Vars', {}, (err, res) => {
            if (err)
                return this.setState({errorMessage: err.message});

            this.setState({vars: res.vars});
        });

        api.on('Var', (err, _var) => {
            const vars = this.state.vars;
            vars.push(_var);
            this.setState({vars});
        });

        api.on('VarEdit', (err, _var) => {
            if (err)
                return this.setState({errorMessage: err.message});

            this.editVar(_var.name, _var.value);
        });

        api.on('VarDelete', (err, _var) => {
            if (err)
                return this.setState({errorMessage: err.message});

            this.deleteVar(_var.name);
        });
    }

    editVar(name, value) {
        const vars = this.state.vars;

        let find = false;

        for (const v of vars) {
            if (v.name === name) {
                v.value = value;
                find    = true;
                break;
            }
        }

        if (!find)
            vars.push({name, value});

        this.setState({vars});
    }

    deleteVar(name) {
        const vars = this.state.vars;

        for (let i = 0; i < vars.length; i++) {
            if (vars[i].name === name) {
                vars.splice(i, 1);
                i--;
            }
        }

        this.setState({vars});
    }

    render() {
        return (
            <div className='col-8'>
                <ul className="list-group" style={{maxHeight: '80vh', overflowY: 'scroll'}}>
                    {this.state.vars.map(v =>
                        <li className='list-group-item' key={Math.random()}>{`${v.key || v.name} = ${v.value}`}</li>
                    )}
                </ul>
            </div>
        );
    }
}

export default Vars;