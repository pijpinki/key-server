import PropTypes from 'prop-types';
import React from 'react';

class VarAction extends React.Component {
    static propTypes = {
        actionLabel: PropTypes.string.isRequired,
        label      : PropTypes.string.isRequired,
        onApply    : PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            name : '',
            value: '',

            errorMessage: null
        };
    }

    /**
     * On change value input
     * @param {event} event
     */
    onChangeValue(event) {
        this.setState({value: event.target.value});
    }

    /**
     * On Change name inout
     * @param {event} event
     */
    onChangeName(event) {
        this.setState({name: event.target.value});
    }


    onApply() {
        const {name, value} = this.state;

        if (!name.length || !value.length)
            return this.setState({errorMessage: 'Check input'});

        this.props.onApply({name, value});
    }

    render() {
        return (
            <div>
                <div className='form'>
                    <span>{this.props.label}</span>

                    {this.state.errorMessage && <span className='text-danger'>{this.state.errorMessage}</span>}

                    <div className='form-group'>
                        <input className='form-control'
                               type='text'
                               placeholder='name'
                               onChange={this.onChangeName.bind(this)}
                        />
                    </div>

                    <div className='form-group'>
                        <input
                            className='form-control'
                            type='text'
                            placeholder='value'
                            onChange={this.onChangeValue.bind(this)}
                        />
                    </div>

                </div>
                <button className='btn btn-primary w-100' onClick={this.onApply.bind(this)}>
                    {this.props.actionLabel}
                </button>
            </div>
        );
    }
}

export default VarAction;