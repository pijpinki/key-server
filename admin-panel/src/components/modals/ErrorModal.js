import PropTypes from 'prop-types';
import React from 'react';

class ErrorModal extends React.Component {
    static propTypes = {
        message: PropTypes.string.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            showClasses   : '',
            disableClasses: 'd-none'
        };
    }

    componentWillReceiveProps(props) {
        if (this.props.open && !props.open)
            this.closeAnimation();

        if (!this.props.open && props.open)
            this.openAnimation();
    }

    closeAnimation() {
        let closeClasses = 'd-block';

        this.setState({closeClasses}, () => {
            closeClasses = 'd-none';

            setTimeout(() => this.setState({closeClasses}), 300);
        });
    }

    openAnimation() {
        let showClasses = 'd-block';

        this.setState({showClasses, disableClasses: showClasses}, () => {
            showClasses += ' show-alert';
            setTimeout(() => this.setState({showClasses}), 300);
        });
    }

    render() {
        return (
            <div className={`alert alert-danger ${this.props.open ? this.state.showClasses : this.state.disableClasses}`} role="alert">
                {this.props.message}
            </div>
        );
    }
}

export default ErrorModal;