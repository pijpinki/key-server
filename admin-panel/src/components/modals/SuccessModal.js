import React from 'react';

class SuccessModal extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            showClasses   : '',
            disableClasses: 'd-none'
        };
    }


    componentWillReceiveProps(props) {
        if (this.props.open && !props.open)
            this.closeAnimation();

        if (!this.props.open && props.open)
            this.openAnimation();
    }

    closeAnimation() {
        let closeClasses = 'd-block';

        this.setState({closeClasses}, () => {
            closeClasses = 'd-none';

            setTimeout(() => this.setState({closeClasses}), 300);
        });
    }

    openAnimation() {
        let showClasses = 'd-block';

        this.setState({showClasses, disableClasses: showClasses}, () => {
            showClasses += ' show-alert';
            setTimeout(() => this.setState({showClasses}), 300);
        });
    }

    render() {
        return (
            <div
                className={`alert-modal alert alert-primary ${this.props.open ? this.state.showClasses : this.state.disableClasses}`}
                role="alert">
                Success
            </div>
        );
    }
}

export default SuccessModal;