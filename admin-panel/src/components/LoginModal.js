import React from 'react';

import api from '../modules/api';

class LoginModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,

            login   : '',
            password: '',

            errorMessage: ''
        };
    }

    componentWillMount() {
        setTimeout(() => this.setState({open: true}), 1000);
    }

    onApply() {
        const {login, password} = this.state;

        if (!login || !password)
            return this.setState({errorMessage: 'Check input data'});

        api.request('Login', {login, password}, err => {
            if (err)
                return this.setState({errorMessage: err.message});

            window.login = true;
            this.setState({open: false});

            this.props.onLogin(true);
        });
    }

    onChangeLogin(event) {
        const login = event.target.value;

        this.setState({login});
    }

    onChangePassword(event) {
        const password = event.target.value;

        this.setState({password});
    }

    render() {
        return (
            <div className={`modal ${this.state.open ? 'd-block' : ''}`} role="dialog">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Modal title</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className='form-group'>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Login"
                                    onChange={this.onChangeLogin.bind(this)}
                                />
                            </div>
                            <div className='form-group'>
                                <input
                                    type="password"
                                    className="form-control"
                                    placeholder="Password"
                                    onChange={this.onChangePassword.bind(this)}
                                />
                            </div>


                            {!!this.state.errorMessage &&
                            <span className='text-danger'>{this.state.errorMessage}</span>}
                        </div>
                        <div className="modal-footer">
                            <button
                                type="button"
                                className="btn btn-primary"
                                onClick={this.onApply.bind(this)}
                            >
                                Login
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default LoginModal;