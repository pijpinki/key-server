/**
 * @typedef {object} event
 * @property {target} target
 */

/**
 * @typedef {object} target
 * @property {*} value
 */