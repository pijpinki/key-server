import React, {Component} from 'react';
import './css/bootstrap.css';
import './css/bootstrap-grid.css';
import './css/bootstrap-reboot.css';
import './App.css';

import NavBar from './components/NavBar';
import LoginModal from './components/LoginModal';
import MainBlock from './components/MainBlock';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            login: false
        };
    }

    onLogin() {
        this.setState({login: true});
    }

    render() {
        return (
            <div className="App">
                <NavBar/>

                <div className='container p-2'>
                    {!this.state.login && <LoginModal onLogin={this.onLogin.bind(this)}/>}

                    {this.state.login && <MainBlock/>}
                </div>
            </div>
        );
    }
}

export default App;
