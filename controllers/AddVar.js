const Handler  = require('./Handler');
const Formater = require('../modules/formater');

/**
 * @api [POST] /var
 *
 * @apiParam {*} name Имя переменной
 * @apiParam {*} [value] Значения перменной
 *
 * @apiResponse {*} var Значения переменной
 */
class AddVar extends Handler {
    onRequest() {
        const {name, value} = this.data;

        if (!name)
            return this.error(new Error('!name'));

        const expires = new Date(new Date().getTime() + 2 * 60 * 1000);

        global.vars[name] = {value, expires};

        if (global.API_SOCKET['admin'])
            global.API_SOCKET['admin'].emit('VarResponse', {name, value, expires});

        this.send({var: Formater.restoreType(global.vars[name]) || null});
    }
}

module.exports = AddVar;