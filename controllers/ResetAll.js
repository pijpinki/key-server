const Handler = require('./Handler');

class ResetAll extends Handler {
    onRequest() {
        global.vars = {};

        this.success();
    }
}

module.exports = ResetAll;