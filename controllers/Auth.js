const config  = require('../config');
const Handler = require('./Handler');

const logger = require('log4js').getLogger('Auth');
logger.level = 'all';

class Auth extends Handler {
    onRequest() {
        const {token} = this.data;

        if (!token)
            return this.error(new Error('Ошибка доступа'));

        if (token.toString() !== config.token.toString())
            return this.error(new Error('Ошибка доступа'));

        this.next();
    }
}

module.exports = Auth;

