const Handler = require('./Handler');
const Formater = require('../modules/formater');

/**
 * @api [PUT] /var
 *
 * @apiParam {*} name
 * @apiParam {*} [value]
 *
 * @apiResponse {*} var
 */
class EditVar extends Handler {
    onRequest() {
        const {name, value} = this.data;

        if (!name)
            return this.error(new Error('!name'));

        global.vars[name] = value || null;

        if(global.API_SOCKET['admin'])
            global.API_SOCKET['admin'].emit('VarEditResponse', {name, value})

        this.send({var: Formater.restoreType(global.vars[name]) || null});
    }
}

module.exports = EditVar;