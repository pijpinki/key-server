const Handler = require('./Handler');

class NotFound extends Handler {
    onRequest() {
        return this.error(new Error('Page not found'));
    }
}

module.exports = NotFound;