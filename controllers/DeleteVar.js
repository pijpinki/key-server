const Handler = require('./Handler');


/**
 * @api [DELETE] /var
 *
 * @apiParam {*} name Имя переменной которую нужно удалить
 *
 * @apiResponse {boolean} success Успех ли
 */
class DeleteVar extends Handler {
    onRequest() {
        const {name} = this.data;

        if (!name)
            return this.error(new Error('!name'));

        try {
            delete global.vars[name];

            if (global.API_SOCKET['admin'])
                global.API_SOCKET['admin'].emit('VarDeleteResponse', {name});

            this.success();
        } catch (err) {
            this.error(err);
        }
    }
}

module.exports = DeleteVar;