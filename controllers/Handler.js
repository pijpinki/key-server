const logger = require('log4js').getLogger('Handler');
logger.level = 'all';

class Handler {
    constructor(req, res, next) {
        this.req  = req;
        this.res  = res;
        this.next = next;

        this.data = Object.assign(this.req.query, this.req.body, this.req.params);

        this.onRequest();
    }

    success() {
        this.send({success: true});
    }

    send(data) {
        this.res.send(data);
    }

    error(error) {
        logger.error(error);
        this.res.status(error.code || error.status || 500).send({message: error.message});
    }
}

module.exports = Handler;