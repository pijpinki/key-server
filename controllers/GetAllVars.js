const Handler = require('./Handler');
const logger  = require('log4js').getLogger('Get all vars');
logger.level  = 'all';


/**
 * @api [GET] /var/all
 */
class GetAllVars extends Handler {
    onRequest() {
        let {offset, limit} = this.data;

        if (!offset)
            offset = 0;

        if (!limit)
            limit = 20;

        const toSend = [];

        const keys   = Object.keys(global.vars);
        const length = keys.length;

        for (let i = offset, j = 0; i < length; i++, j++) {
            if (j > limit)
                break;

            toSend.push({
                key  : keys[i],
                value: global.vars[keys[i]]
            });
        }

        this.send({vars: toSend});
    }
}

module.exports = GetAllVars;