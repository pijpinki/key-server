const Handler  = require('./Handler');
const Formater = require('../modules/formater');

/**
 * @api [GET] /var
 *
 * @apiParam {*} name Имя перенной
 *
 * @apiResponse {*} var
 */
class GetVar extends Handler {
    onRequest() {
        const {name} = this.data;

        if (!name)
            return this.error(new Error('!name'));

        this.send({var: Formater.restoreType(global.vars[name]) || null});
    }
}

module.exports = GetVar;