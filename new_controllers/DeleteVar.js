const Handler = require('./Handler');

class DeleteVar extends Handler {
    onRequest() {
        const {name} = this.data;

        if(!name)
            return this.error(new Error('Missed name'));

        delete global.vars[name];

        return this.send({success: true})
    }
}

module.exports = DeleteVar;