const Handler = require('./Handler');

class GetVar extends Handler {
    onRequest() {
        const {name} = this.data;

        if(!name)
            return this.error(new Error('Missed name'));

        return this.send({exists: !!global.vars[name]})
    }
}

module.exports = GetVar;