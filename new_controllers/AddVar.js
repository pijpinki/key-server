const Handler = require('./Handler');

class AddVar extends Handler {
    onRequest() {
        const {name}  = this.data;
        const expires = new Date(new Date().getTime() + 2 * 30 * 1000);

        if (!name)
            return this.error(new Error('Missed name'));

        global.vars[name] = {name, expires};

        return this.send({success: true})
    }
}

module.exports = AddVar;