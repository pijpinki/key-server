const Handler = require('./Handler');

class ContinueVar extends Handler {
    onRequest() {
        const {name}  = this.data;
        const expires = new Date(new Date().getTime() + 5 * 60 * 1000);

        if (!name)
            return this.error(new Error('Missed name'));

        global.vars[name] = {name, expires};

        return this.send({success: true})
    }
}

module.exports = ContinueVar;