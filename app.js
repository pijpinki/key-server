const express    = require('express');
const app        = express();
const bodyParser = require('body-parser');
const config     = require('./config');
const logger     = require('log4js').getLogger('App');
const socketApp  = require('./socket-server/app');
const async      = require('async');


const connector = require('./modules/connector');

const Auth         = require('./controllers/Auth');
const AddVar       = require('./controllers/AddVar');
const GetVar       = require('./controllers/GetVar');
const EditVar      = require('./controllers/EditVar');
const DeleteVar    = require('./controllers/DeleteVar');
const DeleteAllVar = require('./controllers/ResetAll');
const GetAllWars   = require('./controllers/GetAllVars');
const NotFound     = require('./controllers/NotFound');

const new_GetVar      = require('./new_controllers/GetVar');
const new_AddVar      = require('./new_controllers/AddVar');
const new_ContinueVar = require('./new_controllers/ContinueVar');
const new_DeleteVar   = require('./new_controllers/DeleteVar');

const VarsDelete = require('./modules/VarsDeleters');

logger.level = 'all';

global.vars = {};

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use('/', express.static(`admin-panel/build/`));

app.use(connector(Auth));

// Получить одну переменную
app.get('/var', connector(GetVar));
// Получить список всех перменных
app.get('/var/all', connector(GetAllWars));
// Изменить перменную
app.put('/var', connector(EditVar));
// Добавить перменную
app.post('/var', connector(AddVar));
// Удалить переменную
app.delete('/var/all', connector(DeleteAllVar));
app.delete('/var', connector(DeleteVar));

// new api
app.get('/new/var', connector(new_GetVar));
app.post('/new/var', connector(new_AddVar));
app.put('/new/var', connector(new_ContinueVar));
app.delete('/new/var', connector(new_DeleteVar));

app.use(connector(NotFound));

const startHttpServer = clb => {
    app.listen(config.port, err => {
        err ? logger.fatal('http', err) : logger.info(`SERVER STARTED ON ${config.port}`);
        clb(err);
    });
};

const startSocketServer = clb => {
    socketApp(err => {
        err ? logger.fatal('socket', err) : logger.info(`SOCKET SERVER STARTED ON ${config.socket.port}`);
        clb()
    });
};

new VarsDelete().main();

const functions = [
    startHttpServer.bind(this),
    startSocketServer.bind(this)
];

async.series(functions, err => err ? logger.fatal(err) : logger.info('ALL STARTED SUCCESSFULL'));