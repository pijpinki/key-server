### Запрос Авторизации
#### Нужен для того что бы авторизировать на сокет сервере

+ LoginRequest 
```
{
    required [strings] token : это сесия которая у паса получаеться по api
}
```

+ LoginResponse
```
{
    required [strings] key: '6343080a-999f-4c1b-ac54-2062672f1c52',
    required [int32]   app_id: 1,
    required [int32]   client_id: 5,
    optional [string]  push_type: null,
    optional [string]  push_token: null,
    optional [ISOtime] expires: 2027-12-26T06:30:36.893Z,
    optional [object]  profile: null,
    optional [object]  socials: null,
    optional [ISOtime] last_active: 2017-12-28T06:30:38.261Z,
    optional [ISOtime] created: 2017-12-28T06:30:37.862Z,
    optional [ISOtime] updated: 2017-12-28T06:30:40.160Z,
}
```

+ ErrorResponse Упущен параметр
```
{
   required [int12 (HEX)] code : 0x001
   required [string] message: 'Упущен параметр'
}
```

+ ErrorResponse Сессия не найдена 
```
{
    required [int12 (HEX)] code : 0x100
    required [string] message: 'Сесия не найдена'
}
```

+ ErrorResponse Сессия не авторизована
```
{
   required [int12 (HEX)] code : 0x101
   required [string] message: 'Сесия не авторизвана
}
```