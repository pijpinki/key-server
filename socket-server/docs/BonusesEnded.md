#### Закончииись бонусы
**Response**
+ BonusesEnded
```
{
    required [object] order : {
        required [int32] id
        required [float] cost,
    }
    required [float] needed
    required [float] balance
}
```