const socket_io = require('socket.io');
const path      = require('path');
const http      = require('http');
const logger    = require('log4js').getLogger('SocketServer');
const connector = require('./utils/Connector');

logger.level = 'all';

global.API_SOCKET = {};

class SocketServer {
    constructor(conf) {
        this.config = conf;
        this.server = Object;
    }

    start(clb) {
        logger.info('Запуск сокет сервера');
        if (process.env.ENCRYPTED) {
            logger.debug('https <server>');
            const fs    = require('fs');
            const https = require('https');

            const options = {
                key : fs.readFileSync(path.join(process.cwd(), 'cert', '/privkey.pem')),
                cert: fs.readFileSync(path.join(process.cwd(), 'cert', '/fullchain.pem')),
                ca  : fs.readFileSync(path.join(process.cwd(), 'cert', '/chain.pem'))
            };

            this.server = https.createServer(options);
        } else
            this.server = http.createServer();

        const io = socket_io(this.server);
        io.on('connection', socket => new connector(socket).connect());

        this.server.listen(this.config.port, (err, data) => {
            if (err) {
                logger.fatal(err);

                return clb(err);
            }

            logger.info('Сервер запущен на', this.config.port);
            clb(err, data);
        });
    }
}

module.exports = SocketServer;
