const SocketServer = require('./SocketServer');
const config       = require('../config');


/**
 * Start socket server
 * @param {clb} clb
 */
module.exports = function (clb) {
    const Socket = new SocketServer(config.socket);
    Socket.start(clb)
};