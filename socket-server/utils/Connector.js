const fs     = require('fs');
const logger = require('log4js').getLogger('Connector');
logger.level = 'all';

let handlers = fs.readdirSync(`socket-server/handlers`);

handlers = handlers.map(handler => {
    return {
        name      : handler.split('.')[0],
        controller: require('../handlers/' + handler)
    };
});

handlers = handlers.filter(h => {
    return h.name !== 'Handler';
});

class HandlersLiker {
    constructor(socket) {
        this.socket   = socket;
        this.handlers = handlers;
    }

    connect() {
        logger.debug('Подключился клиент');
        for (const handler of this.handlers) {
            this.socket.on(handler.name, (data) => new handler.controller(this.socket, data));
        }
    }
}

module.exports = HandlersLiker;