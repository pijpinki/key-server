module.exports = {
    // sessions_errors
    SESSION_NOT_FOUND: {
        code: 0x100,
        message: 'Сессия не найдена'
    },

    SESSION_NOT_AUTH: {
        code: 0x101,
        message: 'Сессия не авторизована'
    },

    // request_errors
    MISS_PARAMS: {
        code: 0x001,
        message: 'Упущен параметр'
    }
}