const Handler      = require('./Handler');
const ServerErrors = require('../utils/ServerErrors');

class AddVarRequest extends Handler {
    static get auth() {
        return true;
    }

    onRequest() {
        const {name, value} = this.data;

        if (!name || !value)
            return this.error(ServerErrors.MISS_PARAMS);

        if (global.vars[name])
            return this.send('AddVarResponse', {var: global.vars[name]});

        global.vars[name] = value;

        if (global.API_SOCKET['admin'])
            global.API_SOCKET['admin'].emit('VarResponse', {name, value});

        this.send('AddVarResponse', {var: global.vars[name]});
    }
}

module.exports = AddVarRequest;