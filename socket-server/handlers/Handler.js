const async        = require('async');
const logger       = require('log4js').getLogger('Socket Server');
const ServerErrors = require('../utils/ServerErrors');
logger.level       = 'all';

class Handler {
    constructor(socket, data) {
        this.socket = socket;
        this.data   = data;

        this.preload();
    }

    static getSession(key, clb) {

    }

    auth(clb) {
        if (!this.constructor.auth)
            return clb();

        if (!this.socket.auth)
            return clb(ServerErrors.SESSION_NOT_AUTH);

        clb(null, true);
    }

    preload() {
        const functions = [
            this.auth.bind(this)
        ];

        async.series(functions, err => err ? this.error(err) : this.onRequest());
    }

    error(err) {
        logger.fatal(err);
        this.send('ErrorResponse', {code: err.code || 500, message: err.message});
    }

    send(name, data) {
        logger.debug('send', name, data);
        this.socket.emit(name, data);
    }
}

module.exports = Handler;