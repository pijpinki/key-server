const Handle = require('./Handler');

class PingRequest extends Handle {
    static get auth() {
        return false
    }

    onRequest() {
        this.send('PongResponse')
    }
}

module.exports = PingRequest;