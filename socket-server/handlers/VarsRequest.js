const Handler = require('./Handler');

class VarsRequest extends Handler {
    static get auth () {
        return true;
    }

    onRequest() {
        const toSend = [];

        for (const key in global.vars) {
            toSend.push({name: key, value: global.vars[key]})
        }

        this.send('VarsResponse', {vars: toSend})
    }
}

module.exports = VarsRequest;