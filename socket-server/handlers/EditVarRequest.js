const Handler      = require('./Handler');
const ServerErrors = require('../utils/ServerErrors');

class EditVarRequest extends Handler {
    static get auth() {
        return true;
    }

    onRequest() {
        const {name, value} = this.data;

        if (!name || !value)
            return this.error(ServerErrors.MISS_PARAMS);

        global.vars[name] = value;

        if (global.API_SOCKET['admin'])
            global.API_SOCKET['admin'].emit('VarEditResponse', {name, value});

        this.send('EditVarResponse', {var: global.vars[name]});
    }
}

module.exports = EditVarRequest;