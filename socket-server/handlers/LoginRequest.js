const Handler      = require('./Handler');
const ServerErrors = require('../utils/ServerErrors');
const config       = require('../../config').admin;

class LoginRequest extends Handler {
    static get auth() {
        return false;
    }

    onRequest() {
        const {login, password} = this.data;

        if (!login || !password)
            return this.error(ServerErrors.MISS_PARAMS);

        if (login !== config.login || password !== config.password)
            return this.error(ServerErrors.SESSION_NOT_FOUND);


        global.API_SOCKET['admin'] = this.socket;

        this.socket.auth = true;
        this.send('LoginResponse', {auth: true});
    }
}

module.exports = LoginRequest;