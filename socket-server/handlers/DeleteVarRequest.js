const Handler      = require('./Handler');
const ServerErrors = require('../utils/ServerErrors');

class DeleteVarRequest extends Handler {
    static get auth() {
        return true;
    }

    onRequest() {
        const {name} = this.data;

        if (!name)
            return this.error(ServerErrors.MISS_PARAMS);

        delete global.vars[name];

        if (global.API_SOCKET['admin'])
            global.API_SOCKET['admin'].emit('VarDeleteResponse', {name});

        this.send('DeleteVarResponse', {success: true});
    }
}

module.exports = DeleteVarRequest;