const socket = require('../libs/socket');
const Auth   = require('../../../../tc.api/tests/libs/auth');
const needle = require('needle');
const async  = require('async');

const requestResponse = {
    request: 'LoginRequest', response: 'ErrorResponse'
}

describe('Auth tests', function () {
    this.timeout(10 * 1000);
    describe('Errors Test', function () {
        it('No Token', clb => socket.request(requestResponse, clb))

        it('No session', clb => socket.request(requestResponse, {token: 123}, clb));

        it('Non Auth Session', clb => {
            const session = clb => {
                needle.post(Auth.session, {app_key: 'test'}, (err, res, body) => {
                    if (err)
                        return clb(err)

                    if(res.statusCode !== 200)
                        clb(body)

                    clb(null, res.cookies['tc.api.sid']);
                })
            }

            const test = ({session}, clb) => {
                socket.request(requestResponse, {token: session}, clb)
            }

            async.auto({
                session,
                test: ['session', test]
            }, clb)
        })
    })

    describe('Success Test', () => {
        it('Asuth ok', clb => {
            const auth = clb => {
                Auth.authClient(clb)
            }

            const test = clb => {
                socket.request({request: 'LoginRequest', response: 'LoginResponse'}, {token: Auth.token}, clb);
            }

            async.series([auth, test], clb);
        })
    })
})