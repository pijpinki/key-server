const io = require('socket.io-client')

class Socket {
    constructor(config) {
        this.config = config || {}
        this.socket = io(this.config.url || 'http://localhost:8031')
    }

    /**
     *
     * @param names
     * @param names.request
     * @param names.response
     * @param data
     * @param clb
     */
    request(names, data, clb) {
        if (typeof data === 'function')
            [clb, data] = [data, {}]

        this.socket.emit(names.request, data)

        if (names.response !== 'ErrorResponse')
            this.socket.on('ErrorResponse', err => {
                this.socket.off('ErrorResponse')
                clb(err)
            })

        this.socket.on(names.response, data => {
            this.socket.off(names.response)
            clb(null, data)
        })
    }
}

module.exports = new Socket()