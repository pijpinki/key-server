const network = require('./libs/network');
const config  = require('../config');

const NewVarsApi = new network(`http://localhost:${config.port}/new/var`, {logs: true});
const OldVarsApi = new network(`http://localhost:${config.port}/var`, {logs: true});
const AllVarsApi = new network(`http://localhost:${config.port}/var/all`, {logs: true});

const opts = {token: config.token, auth: true, logs: true};

describe('New api test', function () {
    this.timeout(5 * 60 * 1000);

    it('Add new var', clb => NewVarsApi.successTest('POST', {name: 1}, opts, clb));
    it('Get this var', clb => NewVarsApi.successTest('GET', {name: 1}, opts, clb));
    it('Delete this var', clb => NewVarsApi.successTest('DELETE', {name: 1}, opts, clb));

    it('Add new var for auto delete', clb => NewVarsApi.successTest('POST', {name: 1}, opts, clb));
    it('Must not found var', clb => {
        const request = () =>
            NewVarsApi.successTest('GET', {name: 1}, opts, clb);

        setTimeout(request, 3 * 60 * 1000)
    });

    it('Add new var for auto delete', clb => NewVarsApi.successTest('POST', {name: 1}, opts, clb));
    it('Must continue vars', clb => {
        const request = () =>
            NewVarsApi.successTest('PUT', {name: 1}, opts, clb);

        setTimeout(request, 60 * 1000);
    });
    it('Must return var', clb => {
        const request = () =>
            NewVarsApi.successTest('GET', {name: 1}, opts, clb);

        setTimeout(request, 1.5 * 60 * 1000);
    });
});