const needle = require('needle');

class Network {
    /**
     * @constructor
     *
     * @param {string} url
     * @param {opts} [opts]
     */
    constructor(url, opts) {
        this.url = url;

        this.opts = opts || {};
    }

    /**
     * Make suucess test
     * @param {string} method GET, POST, PUT, DELETE
     * @param {object} data
     * @param {(opts | clb)} opts
     * @param {clb} [clb]
     */
    successTest(method, data, opts, clb) {
        if (clb)
            this.opts = opts;

        if (typeof opts === 'function')
            [clb, opts] = [opts, {}];


        needle(method, this.url, this.makeData(data), (err, res, body) => {
            if (err)
                return clb(err);

            if (res.statusCode !== 200)
                return clb(body);

            if (this.opts.logs)
                console.log('body ->', body);

            clb(null, body);
        });
    }

    /**
     * Make error test
     * @param {string} method GET POST PUT DELETE
     * @param {*} data
     * @param {(opts | clb)} opts
     * @param {clb} [clb]
     */
    errorTest(method, data, opts, clb) {
        if (clb)
            this.opts = opts;

        if (typeof opts === 'function')
            [clb, opts] = [opts, {}];

        needle(method, this.url, this.makeData(data), (err, res, body) => {
            if (err)
                return clb(err);

            if (res.statusCode === 200)
                return clb(body);

            if (this.opts.logs)
                console.log('body ->', body);

            clb(null, body);
        });
    }

    makeData(data) {
        if (this.opts.auth)
            return Object.assign({token: this.opts.token}, data);

        return data;
    }
}

module.exports = Network;

/**
 * @callback clb
 * @param {Error} [error]
 * @param {*} [data]
 */

/**
 * @typedef {Object} opts
 * @property {boolean} [auth]
 * @property {boolean} [logs]
 * @property {*} [token]
 */
