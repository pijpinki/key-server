const network = require('./libs/network');

const config = require('../config');
const opts   = {
    auth : true,
    token: config.token,
    logs : true
};

const Var    = new network(`http://localhost:${config.port}/var`);
const Xyz    = new network(`http://localhost:${config.port}/xyz`);
const VarAll = new network(`http://localhost:${config.port}/var/all`, opts);


describe('key server test', function () {
    describe('Suucess test', function () {
        it('Must get empty var', clb => {
            Var.successTest('GET', {name: 'lol'}, opts, (err, body) => {
                if (err)
                    return clb(err);

                if (body.var !== null)
                    return clb(body);

                clb();
            });
        });

        it('Must write var', clb => {
            Var.successTest('POST', {name: 'lol', value: 1}, opts, (err, body) => {
                if (err)
                    return clb(err);

                if (body.var !== 1)
                    return clb(body);

                clb();
            });
        });

        it('Must get var', clb => {
            Var.successTest('GET', {name: 'lol'}, opts, (err, body) => {
                if (err)
                    return clb(err);

                if (body.var !== 1)
                    return clb(body);

                clb();
            });
        });

        it('Must edit var', clb => {
            Var.successTest('PUT', {name: 'lol', value: 2}, opts, (err, body) => {
                if (err)
                    return clb(err);

                if (body.var !== 2)
                    return clb(err);

                clb();
            });
        });

        it('Must get new var', clb => {
            Var.successTest('GET', {name: 'lol'}, opts, (err, body) => {
                if (err)
                    return clb(err);

                if (body.var !== 2)
                    return clb(body);

                clb();
            });
        });

        it('Must get all vars', clb => {
            VarAll.successTest('GET', {}, clb);
        });

        it('Must delete var', clb => {
            Var.successTest('DELETE', {name: 'lol'}, opts, (err, body) => {
                if (err)
                    return clb(err);

                if (!body.success)
                    return clb(err);

                clb();
            });
        });

        it('Must return null of deleted value', clb => {
            Var.successTest('GET', {name: 'lol'}, opts, (err, body) => {
                if (err)
                    return clb(err);

                if (body.var !== null)
                    return clb(body);

                clb();
            });
        });
    });

    describe('Errors test', () => {
        it('Auth error', clb => {
            Var.errorTest('GET', {name: 'lol'}, {auth: false}, clb);
        });

        it('add var !name', clb => {
            Var.errorTest('POST', {}, opts, (err, body) => {
                if (err)
                    return clb(err);

                if (body.message !== '!name')
                    return clb(body);

                clb();
            });
        });

        it('get var !name', clb => {
            Var.errorTest('GET', {}, opts, (err, body) => {
                if (err)
                    return clb(err);

                if (body.message !== '!name')
                    return clb(body);

                clb();
            });
        });

        it('delete var !name', clb => {
            Var.errorTest('GET', {}, opts, (err, body) => {
                if (err)
                    return clb(err);

                if (body.message !== '!name')
                    return clb(body);

                clb();
            });
        });

        it('not foud', clb => {
            Xyz.errorTest('GET', {}, opts, clb);
        });
    });
});